# imports python librarys for functionality
import json
import sys
import math

# gets the json from the arguments passed to python script
form_data = sys.argv[1]
materialcost_data = sys.argv[2]
shapetime_data = sys.argv[3]
form_obj = json.loads(form_data)
materialcost_obj = json.loads(materialcost_data)
shapetime_obj = json.loads(shapetime_data)
data = {}

# Assigns varibles from json data provided.

# form vars
shape = form_obj["shape"]
width = float(form_obj["width"]) / 1000
height = float(form_obj["height"]) / 1000
length = float(form_obj["length"]) / 1000
c_radius = float(form_obj["c_radius"]) / 1000
c_height = float(form_obj["c_height"]) / 1000
hex_base = float(form_obj["hex_base"]) / 1000
hex_height = float(form_obj["hex_height"]) / 1000

# material vars
price = float(materialcost_obj["price"])
skill_level = int(materialcost_obj["skill_level"])

# shape vars
shapetime = float(shapetime_obj["est_time"])

data['shapetime'] = shapetime
data['skill_level'] = skill_level
# Time Calculations

def skill_time():
    # Skill Levels
    if skill_level == 1:
    	return 10
    if skill_level == 2:
    	return 20
    if skill_level == 3:
    	return 30
    if skill_level == 4:
    	return 80
    if skill_level == 5:
    	return 240

    # Time Calc

basetime = shapetime + skill_time()
data['basetime'] = basetime




# Surface Area for Cuboid
def SA_cuboid():
	LW = width * length
	LH = height * length
	HW = height * width
	SA = LW*2 + LH*2 + HW*2
	# this adds surface area to the data object
	return float(SA)


# Surface Area for Cylinder
def SA_cylinder():
	PI = 3.14
	RH = c_radius * c_height
	R2 = c_radius * c_radius
	SA = PI*RH*2 + PI*R2*2
	# this adds surface area to the data object
	return float(("%.3f" % SA))

def SA_hex():
	# Surface Area for Hexagonal Prism
	SA = (6 * hex_base * hex_height + 3 * math.sqrt(3) * hex_base * hex_base)
	# this adds the surface area of the cylinder to the data object
	return float(("%.3f" % SA))

def calc_area():
	# calculates the area of the surface with the width and height provided.
	area = width * height
	# this adds Area to the data object
	return area


if shape == 'cube' or shape == 'cuboid':
    # Calculate cost of Material per Square Meter.
    cost = price * SA_cuboid()
elif shape == 'cylinder':
    # Calculate cost of Material per Square Meter.
    cost = price * SA_cylinder()
elif shape == 'hexagonal_prism':
    # Calculate cost of Material per Square Meter.
    cost = price * SA_hex()

# this adds Cost to the data object and limits to 2 decimal points
data['cost'] = ("%.2f" % cost)


# takes data object and converts to json
output_data = json.dumps(data)

# pints out json object
print output_data
