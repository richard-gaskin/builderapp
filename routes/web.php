<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'PagesController@index');
Route::get('/quotes', 'PagesController@quotes');
Route::get('/builder', 'PagesController@builder');
Route::get('/requestQuote', 'PagesController@requestQuote');

// Admin or Worker have Access
Route::middleware(['auth','worker'])->group(function () {
    Route::get('/jobs', 'PagesController@jobs');
});

// Admins have Access
Route::middleware(['auth','admin'])->group(function () {
    Route::get('/roles', 'PagesController@roles');
});

// The routes inside this group have a prefix of builder.co.uk/quote/ so ..co.uk/quote/remove
// it also has a middleware of auth , ill show you middleware later
Route::prefix('api/quotes')->middleware(['auth'])->group(function () {
    Route::get('/', 'QuotesController@getUserQuotes');
    Route::post('/build', 'QuotesBuilder@buildQuote');
    Route::post('/save', 'QuotesController@save');
    Route::post('/remove', 'QuotesController@remove');
    Route::post('/purchase','QuotesController@purchase');
});

// Admin or Worker have Access
Route::prefix('api/jobs')->middleware(['auth','worker'])->group(function () {
    Route::get('/', 'JobsController@getJobs');
    Route::get('/pickup/{id}', 'JobsController@assignWorker');
    Route::post('/update','JobsController@updateJobProgress');
});

// Admin or Worker have Access
Route::prefix('api/roles')->middleware(['auth','admin'])->group(function () {
    Route::get('/', 'RolesController@getRoles');
    Route::post('/update','RolesController@updateUserRole');
});

Route::prefix('api/user')->middleware(['auth'])->group(function () {
    Route::get('/id', 'UserController@getUserId');
});
