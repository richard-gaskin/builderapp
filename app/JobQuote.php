<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobQuote extends Model
{
    protected $table = 'job_quotes';


    public function quote()
    {
        return $this->hasOne('App\Quote');
    }

    public function job()
    {
        return $this->belongsTo('App\Job');
    }

}
