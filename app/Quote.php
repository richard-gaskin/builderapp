<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{

    //protected $hidden = ['id','quote_cost'];

    public function job(){
        return $this->hasOne('App\job', 'id', 'quote_id');
    }

}
