<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'user_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function role(){
        return $this->hasOne('App\Roles', 'id', 'role_id');
    }
}
