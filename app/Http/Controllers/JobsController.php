<?php

namespace App\Http\Controllers;


use App\Job;
use Illuminate\Http\Request;
use Auth;



class JobsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Gets all jobs
     *
     * @return mixed
     */
    public function getJobs()
    {
        $jobs = Job::with('worker')->with('quote')->get();

        return $jobs;
    }

    /**
     * Assigns worker to job
     *
     * @param $id
     * @return array
     */
    public function assignWorker($id){

        $job = Job::where('id','=',$id)->with('quote')->first();
        $job->worker_id = Auth::user()->id;
        $job->save();

        $job->worker = Auth::user();

        return ['job_object'=>$job,'status'=>'success'];

    }


    /**
     * Updates job progress
     *
     * @param Request $request
     * @return array
     */
    public function updateJobProgress(Request $request){

        $jobObj = $request->job;

        $jobSql = Job::where('id','=',$jobObj['id'])->with('worker')->with('quote')->first();

        if($jobSql->worker_id == Auth::user()->id){
            if($jobObj['progress'] > $jobSql->progress || $jobObj['done'] != $jobSql->done ) {
                $jobSql->progress = $jobObj['progress'];
                $jobSql->done = $jobObj['done'];
                $jobSql->save();

                return ['status' => 'success','msg'=>'Successfully updated your progress'];
            }else{
                return ['status' => 'error', 'msg'=>'The job percent cannot be set lower than it already is.'];

            }
        }else {
            return ['status' => 'error', 'msg'=>'You are not authorized to preform this action or request.'];
        }
    }


}
