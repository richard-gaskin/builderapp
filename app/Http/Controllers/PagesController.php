<?php

namespace App\Http\Controllers;


use App\Job;
use Illuminate\Http\Request;
use Auth;



class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('home');
    }
    public function quotes()
    {
        return view('quotes');
    }
    public function jobs()
    {
        return view('jobs');
    }
    public function builder()
    {
        return view('builder');
    }
    public function roles()
    {
        return view('roles');
    }

}
