<?php

namespace App\Http\Controllers;

use App\Http\Requests\InputFormRequest;

use App\Job;
use App\Mechanical;
use App\Shape;
use App\Materials;
use App\Tech;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\DB;

class QuotesBuilder extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Gets data from Materials Table where name = what was passed to it
     */
    public function getMaterialCost($material)
    {
        //$materialCost = DB::table('materials')->where('name', $material)->first();
        $materialCost = Materials::where('name', $material)->first();
        return $materialCost;
    }
    public function getShapeTime($shape)
    {
        $shapeTime = Shape::where('name', $shape)->firstOrFail();
        return $shapeTime;
    }
    /**
     * Example of how to run Python Script
     * and echo out the result of it
     */
    public function pyCalcBase($formData, $materialCost, $shapeTime)
    {
        $process = new Process("python /home/builder/web/builder.richardgaskin.co.uk/public_html/script/base_calcs.py '".json_encode($formData)."' '".json_encode($materialCost)."' '".json_encode($shapeTime)."'");
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }

    /**
     * @param InputFormRequest $request
     * @return array
     */
    public function buildQuote(InputFormRequest $request)
    {
        $formData = $request->all();
        // varibles of form data
        $material = $request->input('material');
        $features_mech = $request->input('features_mech');
        $features_tech = $request->input('features_tech');

        // gets cost and time of features from db
        $dbFeatures['mechanical'] = $this->getFeaturesData('App\Mechanical', $features_mech);
        $dbFeatures['tech'] = $this->getFeaturesData('App\Tech', $features_tech);

        // Gets Values for Base Calculation
        $shape = $request->input('shape');
        $materialCost = $this->getMaterialCost($material);
        $shapeTime = $this->getShapeTime($shape);

        // Calculates Base Cost and Time
        $baseCostTime = json_decode($this->pyCalcBase($formData, $materialCost, $shapeTime));

        // Calculate Cost And Time for features
        $mecCostTime = $this->calcFeatureTCTotal($dbFeatures['mechanical']);
        $tecCostTime = $this->calcFeatureTCTotal($dbFeatures['tech']);
        // Adds Cost and Time on
        $baseCostTime->cost += ($mecCostTime['cost'] + $tecCostTime['cost']);
        $baseCostTime->basetime += ($mecCostTime['time'] + $tecCostTime['time']);

        // Add Profiit Margins
        // TODO:: Make Profit Margins Dynamic Depedning on data
        $baseCostTime->cost += 100;

        // gets base hours from minutes
        $baseHours = ceil(($baseCostTime->basetime / 60));

        // gets cost of labor from base hours
        $finalCost = $baseHours * 10 + $baseCostTime->cost;
        // sets cost to updated cost
        $baseCostTime->cost =  ceil($finalCost);
        $baseCostTime->baseHours = $baseHours;

        // time to worker ready
        $basetime = $this->calcDueDate($baseHours);
        $baseCostTime->basetime = preg_replace('/\T/', ' ', $basetime);

        return ['output' => $baseCostTime,'status'=>'success'];
    }

    public function calcDueDate($baseHours){

        // Get jobs from DB
        $jobs = Job::latest('due_date')->first();

        // Create carbon instance of last job due_date
        $date = new Carbon($jobs['due_date']);

        // Set day start and end
        $dayStart = '09:00:00';
        $dayEnd   = '17:00:00';

        // for each hour in base hours
        for($i = 0;$i <= $baseHours;$i++){

            // If the time is not past the day end time and is past the day start time
            if(($date->format('H:i:s') >= $dayEnd) === false && $date->format('H:i:s') >= $dayStart){
                // If is a weekday
                if($this->isWeekday($date)){
                    // Add an hour
                    $date = $date->addHours('1');
                }else{

                }
            }else{
                // if it is a weekend
                if($date->dayOfWeek == 5){
                    // Should be a friday so add 3 days to make it monday
                    $date->addDays('3');
                    // Set Time to 9am
                    $date->hour = 9;
                }else{
                    // Should be a weekday so add 1 day
                    $date->addDays('1');
                    // Set Time to 9am
                    $date->hour = 9;
                }
            }
        }
        return $date;
    }

    private function isWeekday($date){
        switch ($date->dayOfWeek){
            case 0:
                return false;
            break;
            case 1:
                return true;
            break;
            case 2:
                return true;
            break;
            case 3:
                return true;
            break;
            case 4:
                return true;
            break;
            case 5:
                return true;
            break;
            case 6:
                return false;
            break;
        }
    }

    /**
     * Always rounds a number up , default precision is 2 decimal places
     *
     * @param $number
     * @param int $precision
     * @return float|int
     */
    function round_up($number, $precision = 2)
    {
        $fig = (int) str_pad('1', $precision, '0');
        return (ceil($number * $fig) / $fig);
    }

    /**
     * Calculates the total time and cost of all selected features
     *
     * @param $featuresArr
     * @return array
     */
    protected function calcFeatureTCTotal($featuresArr){
        $cost = 0;
        $time = 0;

        foreach ($featuresArr as $key => $featureColl){
            foreach ($featureColl as $feature){
                $cost += $feature->price;
                $time += $feature->est_time;
            }
        }

        return ['cost'=>$cost,'time'=>$time];
    }


    /**
     * Dynamically gets selected features ($selected) from their respected table ($model)
     *
     * @param $model
     * @param $featuresArr
     * @return array
     */
    private function getFeaturesData($model,$featuresArr){

        $sqlArr = [];

        foreach($featuresArr as $feature => $bool) {
            if($bool === true) {
                $sqlArr[$feature] = $model::where('name', '=', $feature)->get();
            }
        }
       return $sqlArr;
    }

}
