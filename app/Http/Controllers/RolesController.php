<?php

namespace App\Http\Controllers;


use App\Role;
use Illuminate\Http\Request;
use Auth;



class RolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Gets all jobs
     *
     * @return mixed
     */
    public function getRoles()
    {
        //$jobs = Role::all();
        //dd($jobs);
        $jobs = Role::with('user')->with('role')->get();
        return $jobs;
    }

    /**
     * Updates job progress
     *
     * @param Request $request
     * @return array
     */
    public function updateUserRole(Request $request){

        $userObj = $request->user;
        //var_dump($userObj);
        $userSql = Role::where('id','=',$userObj['id'])->with('user')->with('role')->first();
        //var_dump($userSql);

        if($userSql->user->id != Auth::user()->id){
            $userSql->role_id = $userObj['role_id'];
            $userSql->save();
            return ['status' => 'success', 'msg'=>'Role has successfully been updated'];
        }else {
            return ['status' => 'error', 'msg'=>'You are trying To update you\'re own role, this isn\'t allowed as you will lock yourself out'];
        }
    }

}
