<?php

namespace App\Http\Controllers;


use App\Job;
use Illuminate\Http\Request;
use Auth;



class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Gets User Id
     *
     * @return mixed
     */
    public function getUserId()
    {
        $userId = Auth::user()->id;
        return $userId;
    }

}
