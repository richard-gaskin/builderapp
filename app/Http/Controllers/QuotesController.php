<?php

namespace App\Http\Controllers;


use App\Http\Requests\InputFormRequest;
use App\Http\Requests\RemoveQuote;
use App\Job;
use App\Quote;
use Illuminate\Http\Request;
use Auth;


class QuotesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Gets all Users Quotes
     *
     * @return mixed
     */
    public function getUserQuotes()
    {

        $quotes = Quote::where('customer_id', Auth::user()->id)->get();

        foreach ($quotes as $quote) {
            $quote->job = Job::where('quote_id', '=', $quote->id)->first();
        }

        return $quotes->toJson();
    }

    /**
     * Delete Quote
     *
     * @param RemoveQuote $request
     * @return array
     */
    public function remove(RemoveQuote $request)
    {
        $quote = Quote::where('id', '=', $request->quote_id)->first();

        // if the quote belongs to user
        if ($quote->customer_id == Auth::user()->id) {
            Quote::destroy($quote->id);
            return ['status' => 'success'];
        } else {
            return ['status' => 'error', 'msg' => 'You are not authorized to preform this action or request'];
        }

    }

    /**
     * @param InputFormRequest $request
     * @return array
     *
     */
    public function save(InputFormRequest $request)
    {

        $quotesBuilder = new QuotesBuilder();
        $quoteOutput = json_encode($quotesBuilder->buildQuote($request));

        $quoteJson = json_decode($quoteOutput);
        $jobSpec = json_encode($request->all());

        //Checks if Quote ID exists
        $quote = Quote::where('id', '=', $request->id)->first();
        if ($quote !== null) {
            // Checks if it belongs to user

            if ($quote->customer_id == Auth::user()->id) {
               /// var_dump($jobSpec);

                $updateDetails = [
                    'job_cost' => $quoteJson->output->cost,
                    'job_spec' => $jobSpec,
                    'quote_cost' => $quoteJson->output->cost,
                    'job_time' => $quoteJson->output->basetime
                ];

                Quote::where('id', $request->id)
                    ->update($updateDetails);
            }
        } else {
            $save_quote = new Quote();
            $save_quote->customer_id = Auth::user()->id;
            $save_quote->job_cost = $quoteJson->output->cost;
            $save_quote->job_spec = $jobSpec;
            $save_quote->quote_cost = $quoteJson->output->cost;
            $save_quote->job_time = $quoteJson->output->basetime;
            $save_quote->notes = 'this is a test';

            $save_quote->save();
            return ['status' => 'success', 'output' => 'Saved'];
        }
    }


    /**
     * purchase the given quote
     */
    public function purchase(Request $request)
    {

        $quote = $request->quote;
        if ($quote['customer_id'] == Auth::user()->id) {
            if ($quote['purchased'] == 0) {
                $quoteSql = Quote::where('id', '=', $quote['id'])->first();
                $quoteSql->purchased = 1;
                $quoteSql->save();

                $job = new Job();
                $job->customer_id = Auth::user()->id;
                $job->progress = 0;
                $job->worker_id = 0;
                $job->quote_id = $quote['id'];
                $job->done = 0;

                $job_spec  = json_decode($quote['job_spec']);
                $job->due_date = $this->getDueDate($job_spec->baseCostTIme->baseHours);
                $job->save();

                $jobSql = Job::where('id', '=', $job->id)->with('quote')->first();

                return ['status' => 'success', 'job' => $jobSql];
            } else {
                return ['status' => 'error', 'msg' => 'This Quote has already been purchased'];
            }
        } else {
            return ['status' => 'error', 'msg' => 'You are not authorized to preform this action or request'];
        }

    }

    public function getDueDate($baseHours){

        $quotesBuilder = new QuotesBuilder();
        //dump($baseHours);
        $date = $quotesBuilder->calcDueDate($baseHours);
        $date = preg_replace('/\T\Z/', ' ', $date);
        return $date;

    }

}
