<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role->role_id == 1) {
            return $next($request);
        }
        return abort(404);
    }
}
