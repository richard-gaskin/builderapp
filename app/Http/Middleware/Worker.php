<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Worker
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role_id = Auth::user()->role->role_id;
        // Checks if user is a worker or admin and if so
        // lets them pass otherwise throw a 404 Page
        if ($role_id == 2) {
            return $next($request);
        }
        elseif ($role_id == 1) {
            return $next($request);
        }
        return abort(404);
    }
}
