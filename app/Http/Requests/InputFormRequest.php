<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InputFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'width' => 'required|numeric|digits_between:0,6',
           'height' => 'required|numeric|digits_between:0,6',
        ];
    }

    public function messages()
    {
        return [
            'width.required' => 'Width is required',
            'width.numeric' => 'Width needs to be the number',
            'width.digits_between' => 'Your width is too long',
            'height.required'  => 'Height is required',
            'height.numeric'  => 'Height needs to be the number',
            'height.digits_between' => 'Your height is too long',
        ];
    }



}
