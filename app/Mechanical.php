<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mechanical extends Model
{
    protected $table = 'features_mechanical';
}
