<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{

    public function worker(){
        return $this->hasOne('App\User', 'id', 'worker_id');
    }

    public function quote(){
        return $this->hasOne('App\Quote','id','quote_id');
    }

}
